# ChartFactory front-end code

## Description

This repo contains the front-end code for [ChartFactory](http://chartfactory.com/ "ChartFactory").

## Install

This code depends on a number of 3rd party libraries. See bower.conf???

## Developers

Developers write code:
    tell application "Foo"
        beep
    end tell

### Cloning the repo

* step 1
* step 2
* step 3

### Branch policy

This is decribed in Evernote: git notes

### Pushing changes

* git add <file>
* git commit -m "<comment>"
* git push -u origin master


