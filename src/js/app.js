angular.module('sockio', [])
    .factory('sockioService', ['$rootScope', '$location',  
			       function($rootScope, $location) {
				   var socket = io.connect($location.host() + '/btc');
				   return {
				       on: function (eventName, callback) {
					   socket.on(eventName, function () {
					       var args = arguments;
					       $rootScope.$apply(function () {
						   callback.apply(socket, args);
					       });
					   });
				       },
				       emit: function (eventName, data, callback) {
					   socket.emit(eventName, data, function () {
					       var args = arguments;
					       $rootScope.$apply(function () {
						   if (callback) {
						       callback.apply(socket, args);
						   }
					       });
					   })
				       }
				   };
			       }
			      ]);


var cfcApp = angular.module('cfcApp', ['ui.bootstrap', 'sockio', 'ngRoute']);

cfcApp.run(['$rootScope', '$log', function($rootScope, $log){
    $rootScope.$log = $log;
}]);

cfcApp.config(['$logProvider', function($logProvider){
    $logProvider.debugEnabled(true); // debugging log can be switched off here
}]);

cfcApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $routeProvider.
	when('/about', {
	    templateUrl: 'about.html',
	}).
	when('/contact', {
	    templateUrl: 'contact.html',
	}).
	when('/disclaimer', {
	    templateUrl: 'disclaimer.html',
	}).
	otherwise({
	    redirectTo: '/'
	});
}]);

cfcApp.directive('hist', ['$window', '$timeout', '$log', '$filter', 'sockioService',
			     function($window, $timeout, $log, $filter, sockioService) {
				 return {
				     restrict: 'A',
				     scope: {
					 data: '='
				     },
				     link: function(scope, ele, attrs) {
					 var exch_id = parseInt(attrs.exchId) || 0;
                                         var exch, label;
					 var width, prev_width;
                                         var chart, timeSeries, canv;

					 function render() {
                                                 
					     width = document.getElementById('histcharts').clientWidth;

					     if (width != prev_width) {
						 prev_width = width;

                                                 ele.html('<canvas id="canv' + exch_id + '" width="' + width + '" height="60"></canvas><span id="histlabel' + exch_id + '" class="histlabel"></span>');

                                                 canv = document.getElementById('canv'+exch_id);
                                                 label = document.getElementById('histlabel'+exch_id);

                                                 ele[0].width = width;
                                                 var initDone = false;

                                                 chart = new SmoothieChart({millisPerPixel:5000, scaleSmoothing:1, timestampFormatter:SmoothieChart.timeFormatter, grid: {millisPerLine: 200000, sharpLines: false}});
                                                 timeSeries = new TimeSeries();
                                                 chart.addTimeSeries(timeSeries, {lineWidth:2, strokeStyle:'#00ff00'});
                                         

                                                 var num_datapoints = width
                                                 sockioService.emit('getHistory', {exch:exch_id, num:num_datapoints});
					         ////$log.debug("Requesting getHistory for datapoints: " + num_datapoints)

                                                 var unregister = scope.$watch('data.init', function(newData) {
                                                     if ((typeof newData !== 'undefined') && ('hist' in newData) && (newData.hist.length > 0)) {
                                                         histData = newData.hist;
                                                         exch = newData.exch;
                                                         document.getElementById('histlabel'+exch_id).innerHTML = exch;
                                                         for (var i=histData.length-1; i>=0; i--){
                                                             ////$log.debug(histData[i][0], histData[i][1]);                                                     
                                                             timeSeries.append(histData[i][0]*1000, histData[i][1]);   
                                                             var endTime = histData[i][0]*1000;
	    					         }; 
                                                         chart.render(canv, endTime);                                                        
                                                         
                                                         initDone = true;
                                                         chart.streamTo(canv, 100);
                                                     }
                                                 })                                                         

                                                 
                                                 scope.$watch('data.last', function(newData) {
                                                     if (typeof newData !== 'undefined' && newData.length > 0) {
                                                         if (initDone) {
                                                             timeSeries.append(newData[0], newData[1]);
                                                             if (typeof(unregister) == 'function') {
                                                                 unregister();
						                 ////$log.debug('called unregister()');
						                 unregister = undefined;
                                                             }
                                                         }
                                                     }
                                                 }, true);
                                             }   
                                         }

                                         render();

                                         // render on resize again (and keep other listeners)
                                         var existing = $window.onresize;
                                         $window.onresize = function() {
                                             if (existing) {
                                                 existing();
                                             }
                                             render();
                                         }

					 sockioService.on('reconnect', function() {
					     ////$log.debug('Render history again');
                                             render();
					 })

				     } // end link function
				 } // end return directive obj
			     } // end directive function
			    ])


cfcApp.directive('d3Donut', ['$window', '$timeout', '$log', '$filter',
			     function($window, $timeout, $log, $filter) {
				 return {
				     restrict: 'A',
				     scope: {
					 data: '='
				     },
				     link: function(scope, ele, attrs) {

					 $timeout(function(){
					     var width = document.getElementById('donut2').clientWidth,
					     height = width;

					     ////$log.debug('width: ' + width)

					     var outerRadius = Math.min(width,height)/2 - 5,
					     innerRadius = (outerRadius/50)*42;

					     ////$log.debug('outerRadius: ' + outerRadius);
					     ////$log.debug('innerRadius: ' + innerRadius);

					     var exch_id = parseInt(attrs.exchId) || 0;
					     var τ = 2 * Math.PI;
					     var foreground;

					     scope.$watch('data', function(newData) {

						 ////$log.debug("scope.$watch triggered with data: ", newData, JSON.stringify(newData));
						 if (!newData) { return; }

						 if (!foreground) {
						     // First render
						     ////$log.debug('First render: ', newData, JSON.stringify(newData));
						     scope.render(newData);
						 } else {
						     // Update 
						     ////$log.debug('Update: ', newData, JSON.stringify(newData));
						     scope.update(newData);
						 }                        
					     }, true);

					     var arc = d3.svg.arc()
						 .innerRadius(innerRadius)
						 .outerRadius(outerRadius)
						 .startAngle(0);

					     var xtransform = Math.min(width,height) / 2 ;
					     ////$log.debug('xtransform: ' + xtransform);
					     
					     var svg = d3.select(ele[0]).append("svg")
						 .attr("width", '100%')
						 .attr("height", '100%')
						 .attr('viewBox','0 0 '+Math.min(width,height) +' '+Math.min(width,height) )
						 .attr('preserveAspectRatio','xMinYMin')
						 .append("g")
						 .attr("transform", "translate(" + xtransform + "," + Math.min(width,height) / 2 + ")");
					     
					     // Add the background arc, from 0 to 100% (τ).
					     var background = svg.append("path")
						 .datum({endAngle: τ})
						 .style("fill", "#4f4f4f")
						 .attr("d", arc);

					     scope.render = function(data) {                                       
						 // Add the foreground arc
						 foreground = svg.append("path")
						     .datum({endAngle: data.volat/data.vmax * τ})
						     .style("fill", "#f2f2f2")
						     .attr("d", arc);

						 // Add the text field in the center of the donut
						 var makenum = $filter('number');                        
						 var text = svg.append("text")
						     .attr("text-anchor", "middle")
						     .attr("dy", "0.35em")
						     .text(makenum(data.volat, 2) + '%')
						     .attr("class", 'volat');
					     }


					     scope.update = function(data) {               
						 // Update the foreground arc
						 foreground.transition()
						     .duration(750)
						     .call(arcTween, data.volat/data.vmax * τ);
						 // Update the text field
						 var makenum = $filter('number');
						 svg.selectAll('text').transition().text(makenum(data.volat, 2) + '%')
					     }

					     function arcTween(transition, newAngle) {
						 transition.attrTween("d", function(d) {
						     var interpolate = d3.interpolate(d.endAngle, newAngle);
						     return function(t) {
							 d.endAngle = interpolate(t);
							 return arc(d);
						     };
						 });
					     }

					 }, 100) // end $timeout callback



				     } // end link function
				 } // end return directive obj
			     } // end directive function
			    ])


cfcApp.directive('d3Pies', ['$window', '$timeout', '$log',
			    function($window, $timeout, $log) {
				return {
				    restrict: 'A',
				    scope: {
					data: '='
				    },
				    link: function(scope, ele, attrs) {

					var radius = parseInt(attrs.pieRadius) || 80;
					var width = radius*2 + 20;
					var height = radius*2


					var synchronizedMouseOver = function() {
					    var arc = d3.select(this);
					    var indexValue = arc.attr("index_value");                        
					    var arcSelector = ".arc-" + indexValue;
					    var selectedArc = d3.selectAll(arcSelector);
					    selectedArc.style("fill", "#E89640");                        
					    var bulletSelector = ".legendBullet-" + indexValue;
					    var selectedLegendBullet = d3.selectAll(bulletSelector);
					    selectedLegendBullet.style("fill", "#E89640");                        
					    var textSelector = ".legendText-" + indexValue;
					    var selectedLegendText = d3.selectAll(textSelector);
					    selectedLegendText.style("fill", "#E89640");
					};

					var synchronizedMouseOut = function() {
					    var arc = d3.select(this);
					    var indexValue = arc.attr("index_value");                        
					    var arcSelector = ".arc-" + indexValue;
					    var selectedArc = d3.selectAll(arcSelector);
					    var colorValue = selectedArc.attr("color_value");
					    selectedArc.style("fill", colorValue);        
					    var textSelector = ".legendText-" + indexValue;
					    var selectedLegendText = d3.selectAll(textSelector);
					    selectedLegendText.style("fill", "#9a9a9a");
					};

					// Store the displayed angles in _current.
					// Then, interpolate from _current to the new angles.
					// During the transition, _current is updated in-place by d3.interpolate.
					function arcTween(a) {
					    var i = d3.interpolate(this._current, a);
					    this._current = i(0);
					    return function(t) {
						return arc(i(t));
					    };
					}

					function kFormatter(num) {
					    return num > 999 ? (num/1000).toFixed(1) + 'K' : num
					}
					
					var pie  = d3.layout.pie() // get a pie object structure
					    .value(function(d) { return d.volume; })
					    .sort(null); // disable sort-by-value
					var arc = d3.svg.arc()
					    .innerRadius(0)
					    .outerRadius(radius);
					var svg = d3.select(ele[0]).append("svg")
					    .attr("width", width)
					    .attr("height", height)
					    .append("g")
					    .attr("transform", "translate(" + radius + "," + radius + ")");
					var legend = d3.select(ele[0]).append("svg")
					    .attr("width", radius + 20)
					    .attr("height", radius * 2);

					scope.$watch('data', function(newData) {
					    ////$log.debug("scope.$watch triggered with data: ", newData, JSON.stringify(newData));
					    if (!newData.length) { return; }

					    if (newData.length > svg.selectAll('g.arc').size()) {
						// First render
						////$log.debug('First render: ', newData, JSON.stringify(newData));
						scope.render(newData);
					    } else {
						// Update 
						////$log.debug('Update: ', newData, JSON.stringify(newData));
						scope.update(newData);
					    }
					}, true);
					
					scope.render = function(data) {               

					    ////$log.debug("Data from RENDER function: ", data, JSON.stringify(data));
					    svg.selectAll('g').remove();
					    legend.selectAll('g').remove();

					    var color = d3.scale.ordinal()
						.domain(data)
						.range(["#f7f7f7","#cccccc","#969696","#525252"]);
					    var txtcolor = d3.scale.ordinal()
						.domain(data)
						.range(["#2b2b2b","#2b2b2b","#2b2b2b","#f2f2f2"]);
					    var arcs = svg.selectAll("g.arc")
						.data(pie(data))
						.enter().append("g");
					    var path = arcs.append("path")
						.attr("fill", function(d, i) { return color(i); })
						.attr("d", arc)
						.attr("index_value", function(d, i) { return "index-" + i; })
						.attr("class", function(d, i) { return "arc-index-" + i; })
						.on('mouseover', synchronizedMouseOver)
						.on("mouseout", synchronizedMouseOut)
						.each(function(d) { this._current = d; }); // store the initial angles
					    var text = arcs.append("text")
						.attr("transform", function(d) {
						    return "translate(" + arc.centroid(d) + ")";
						})
						.text(function(d,i) {
						    return kFormatter(data[i].volume);
						})
						.style("font-size", '12px')
						.attr("fill", function(d, i) { return txtcolor(i); })
						.attr("font-family", '"Helvetica Neue", Helvetica, Arial, sans-serif')
						.attr("text-anchor", "middle"); //center the text on it's origin
					    
					    var legends = legend
						.selectAll("g")
						.data(data)
						.enter().append("g")
						.attr("transform", function(d, i) { return "translate(0," + 20*i + ")"; });

					    legends.append("rect")
						.attr("width", 16)
						.attr("height", 16)
						.style("fill", function(d, i) { return color(i); });

					    legends.append("text")
						.attr("x", 22)
						.attr("y", 9)
						.attr("dy", ".35em")
						.style("font-family", '"Helvetica Neue", Helvetica, Arial, sans-serif')
						.style("font-size", '14px')
						.style("fill", '#9a9a9a')
						.text(function(d,i) { return data[i].exch; })
						.attr("index_value", function(d, i) { return "index-" + i; })
						.attr("class", function(d, i) { return "legendText-index-" + i;})
						.on('mouseover', synchronizedMouseOver)
						.on("mouseout", synchronizedMouseOut);

					} // end scope.render

					scope.update = function(data) {

					    ////$log.debug("Data from UPDATE function: ", data, JSON.stringify(data));
					    ////$log.debug(svg.selectAll('rect').length);
					    path
						.data(pie(data))
						.transition()
						.duration(750)
						.attrTween("d", arcTween);
					} // end scope.update


				    } // end link function
				} // end return directive obj
			    } // end directive function
                           ])


cfcApp.directive('d3Bars', ['$window', '$timeout', '$log',
			    function($window, $timeout, $log) {
				return {
				    restrict: 'A',
				    scope: {
					data: '='
				    },
				    link: function(scope, ele, attrs) {
					var w = attrs.chartWidth || '100%';
					var h = parseInt(attrs.chartHeight) || 160;
					var clr = attrs.baseColor || 'steelblue';
					var xScale, yScale;

					var svg = d3.select(ele[0])
					    .append('svg')
					    .attr('width', w)
					    .attr('height', h);



					scope.$watch('data', function(newData) {
					    if (!newData.length) { return; }
					    ////$log.debug("scope.$watch triggered with data: ", newData, JSON.stringify(newData));
					    if (newData.length > svg.selectAll('rect').size()) {
						// First render
						////$log.debug('First render: ', newData, JSON.stringify(newData));
						scope.render(newData);
					    } else {
						// Update 
						////$log.debug('Update: ', newData, JSON.stringify(newData));
						scope.update(newData);
					    }
					}, true);

					
					scope.setxScale = function(data) {
					    var max = d3.max(data, function(d) { return d.price; });
					    var min = d3.min(data, function(d) { return d.price; });
					    xScale = d3.scale.linear()
						.domain([min*.99, max])
						.range([0, w]);
					}
					scope.setyScale = function(data) {
					    yScale = d3.scale.ordinal()
						.domain(d3.range(data.length))
						.rangeRoundBands([0, h], 0.03);
					}

					scope.render = function(data) {               

					    ////$log.debug("Data from RENDER function: ", data, JSON.stringify(data));
					    svg.selectAll('*').remove();

					    scope.setxScale(data);
					    scope.setyScale(data);
					    svg.selectAll('rect')
						.data(data)
						.enter()
						.append('rect')
						.attr('y', function(d, i) {
						    return yScale(i);
						})
						.attr('x', 0)
						.attr('height', yScale.rangeBand())
						.attr('width', function(d) { return xScale(d.price) })
						.attr('fill', clr)
						.style("fill-opacity", function(d,i) { return 0.6+i*0.1; });
					} // end scope.render

					scope.update = function(data) {

					    ////$log.debug("Data from UPDATE function: ", data, JSON.stringify(data));
					    ////$log.debug(svg.selectAll('rect').length);

					    scope.setxScale(data);
					    svg.selectAll('rect')
						.data(data)
						.transition()
						.duration(500)
						.attr('width', function(d) { return xScale(d.price) });
					} // end scope.update

				    } // end link function
				} // end return directive obj
			    } // end directive function
                           ])
    .controller('ChartCtrl', ['$scope', '$filter', '$log', 'sockioService', 
			      function($scope, $filter, $log, sockioService) {
				  $scope.data = {};
				  $scope.data.last = [];
				  $scope.data.high = [];
				  $scope.data.low = [];
				  $scope.data.volume = [];
				  $scope.data.bidask = [];
				  $scope.data.volat = [];
				  $scope.data.avg = {};
				  $scope.histdata = [];
				  $scope.histdata[0] = {};
				  $scope.histdata[1] = {};
				  $scope.histdata[2] = {};
				  $scope.histdata[3] = {};
				  $scope.histdata[0].last = [];
				  $scope.histdata[1].last = [];
				  $scope.histdata[2].last = [];
				  $scope.histdata[3].last = [];

                                  sockioService.on('history', function(data) {
				      ////$log.debug('Received history: ', data, JSON.stringify(data));
                                      $scope.histdata[data.id].init = {exch:data.exch, hist:data.hist};
                                  });

				  sockioService.on('last:ticker', function(data) {
				      
				      ////$log.debug('UPDATED ticker: ', data, JSON.stringify(data));

				      $scope.data.last[data.id] = {price:data.last, exch:data.exch};
				      $scope.data.high[data.id] = {price:data.high, exch:data.exch};
				      $scope.data.low[data.id] = {price:data.low, exch:data.exch};
				      $scope.data.volume[data.id] = {volume:data.volume, exch:data.exch};
				      $scope.data.bidask[data.id] = {bid:data.bid, ask:data.ask, exch:data.exch};
				      $scope.histdata[data.id].last = [data.utc_ts*1000, data.last/100];
				  });

				  sockioService.on('last:avg', function(data) {                       
				      ////$log.debug('UPDATED avg: ', data, JSON.stringify(data));
				      var pricestr = data.last.toString();
				      var last = parseInt(pricestr);
				      var str24h = data.last24h.toString();
				      var last24h = parseInt(str24h);
				      var diffabs = last - last24h;
				      if (diffabs == 0) {
					  $scope.data.avg['updown'] = '=';
					  $scope.data.avg['achng'] = 0
					  $scope.data.avg['chng'] = ['0', '00'];
				      } else {
					  $scope.data.avg['updown'] = (diffabs > 0) ? '+' : '-';
					  $scope.data.avg['achng'] = Math.abs(diffabs/100);
					  var diffperc = (last/last24h - 1) * 100;
					  diffperc = diffperc.toString().split('.')
					  $scope.data.avg['chng'] = [diffperc[0], diffperc[1].substring(0,2)];
				      }
				      $scope.data.avg['last'] = [pricestr.slice(0,-2), pricestr.slice(-2)];
				  });

				  sockioService.on('volat:ticker', function(data) {                      
				      ////$log.debug('UPDATED volat ticker: ', data, JSON.stringify(data));
				      $scope.data.volat[data.id] = {volat:data.volat, vmax:data.vmax, exch:data.exch};
				  });
			      }]);

cfcApp.controller('AlertCtrl', ['$scope', '$log', 'sockioService', '$window', function ($scope, $log, sockioService, $window) {
    // https://github.com/LearnBoost/socket.io/wiki/Exposed-events
    $scope.alert = {};
    $scope.alert.text = '';
    $scope.alert.class = '';

    sockioService.on('connecting', function() {
	//$scope.alert.text = 'Connection to the server...';
	//$scope.alert.class = 'info';
	//$window.scrollTo(0, 0);
        ////$log.debug('On connecting fired');
    })
    sockioService.on('connect', function() {
        sockioService.emit("login", 'hello'); // Wake up server (maybe not needed)
	$scope.alert.text = '';
	$scope.alert.class = '';
	$window.scrollTo(0, 0);
        ////$log.debug('On connect fired');
    })
    sockioService.on('connect_failed', function () {
	$scope.alert.text = "Can't establish a connection to the server. Try a hard refresh on your browser or try again later.";
	$scope.alert.class = 'error';
	$window.scrollTo(0, 0);
        ////$log.debug('On connect_failed fired');
    })
    sockioService.on('disconnect', function() {
	$scope.alert.text = 'Lost connection with the server, please wait...';
	$scope.alert.class = 'error';
	$window.scrollTo(0, 0);
        ////$log.debug('On disconnect fired');
    })
    sockioService.on('reconnecting', function(reconnectionDelay, reconnectionAttempts) {
	var retry = reconnectionDelay/1000;

	$scope.alert.text = 'Lost connection, reconnecting in ' + retry + ' second' + (retry==1 ? '' : 's') + '...';
	$scope.alert.class = 'warning';
	$window.scrollTo(0, 0);
        ////$log.debug('reconnecting now, delay:' + reconnectionDelay + ', attempts:' + reconnectionAttempts);
    })
    sockioService.on('reconnect_failed', function () {
	$scope.alert.text = "Can't establish a connection to the server. Try a hard refresh on your browser or try again later.";
	$scope.alert.class = 'error';
	$window.scrollTo(0, 0);
        ////$log.debug('On reconnect_failed fired');
    })
    sockioService.on('reconnect', function() {
	$scope.alert.text = '';
	$scope.alert.class = '';
	$window.scrollTo(0, 0);
	////$log.debug('On reconnect fired');
    })
    sockioService.on('error', function() {
	$scope.alert.text = "Can't establish a connection to the server. Try a hard refresh on your browser or try again later.";
	$scope.alert.class = 'error';
	$window.scrollTo(0, 0);
        ////$log.debug('On error fired');
    })

}]);

cfcApp.controller('CollapseCtrl', ['$scope', '$window', '$location', 
				   function($scope, $window, $location) {
				       $scope.isCollapsed = true;
				       $scope.alert = {};
				       $scope.alert.class = 'info';
				       
				       $scope.$on('$locationChangeSuccess', function(event) {
					   if (!$scope.isCollapsed) {
					       $scope.isCollapsed = true;
					   }
					   $window.scrollTo(0, 0);
				       });
				   }]);

cfcApp.controller('DateCtrl', ['$scope', function($scope) {
    $scope.dt = new Date();
}]);
