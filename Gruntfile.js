module.exports = function(grunt) {

    // custom spul: mergen, minifyen en link replacen in index.html
    // 1 JS file en 1 CSS file
    // alles wat door Google of anderen wordt gehost:
    // link replacen in index.html


/*


AngularJS
    snippet: <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.10/angular.min.js"></script> 
    site: http://angularjs.org 
    stable versions: 1.2.10, 1.2.9, 1.2.8, 1.2.7, 1.2.6, 1.2.5, 1.2.4, 1.2.3, 1.2.2, 1.2.1, 1.2.0, 1.0.8, 1.0.7, 1.0.6, 1.0.5, 1.0.4, 1.0.3, 1.0.2, 1.0.1 
    unstable versions: 1.2.0-rc.3, 1.2.0-rc.2, 1.2.0rc1, 1.1.5, 1.1.4, 1.1.3 

    ng-route: http://ajax.googleapis.com/ajax/libs/angularjs/1.2.10/angular-route.min.js

    http://cdnjs.com/libraries/angular-ui-bootstrap/ (maar beter custom gebruiken en mergen met app.js)

D3js
    <script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>

    Sites using HTTPS may wish to self-host D3, or use a CDN that supports HTTPS, such as CDNJS. 
    https://cdnjs.com/libraries/d3/

Bootstrap
    Bootstrap CDN

    The folks over at MaxCDN graciously provide CDN support for Bootstrap's CSS and JavaScript. Just use these Bootstrap CDN links.

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">


Socket.io
    http://cdnjs.com/libraries/socket.io/

    And rewrite this:
    <script>
     WEB_SOCKET_SWF_LOCATION = "bin/WebSocketMain.swf";
     WEB_SOCKET_DEBUG = true;
    </script>
    http://cdn.socket.io/stable/WebSocketMain.swf

custom.css
    Evt inline in HTML proppen

Multi:
    http://www.jsdelivr.com/
    http://cdnjs.com/


Samengevat:
+ index.html met custom.css erin > EC2*
+ bootstrap.css > BS CDN
+ Socketio.js > CDNJS
+ app.js + ng-bootstrap-custom.js > S3*
+ d3js > CDNJS
+ ng.js > google cdn
+ ng-route.js > google cdn
+ WEB_SOCKET_SWF_LOCATION = "bin/WebSocketMain.swf" > S3*

* deploy met Fabric


*/

  // get command line option --target
  var target = grunt.option('target') || 'stage';
  var s3 = (target == 'prod') ? '//s3.amazonaws.com/chartfactory/' : '//s3.amazonaws.com/stage_chartfactory/';


  // Project configuration.
  grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
      conf: {
          temp: '.tmp',
	  s3: s3,
          srcjs: 'src/js/',
      },
      clean: {
          init: ["build", "build/js"],
          exit: ['<%= conf.temp %>','build/css']
      },
      copy: {
          html: {
              src: 'src/index.html',
              dest: 'build/index.html'
          },
          fav: {
              src: 'src/favicon.ico',
              dest: 'build/favicon.ico'
          },
          rob: {
              src: 'src/robots.txt',
              dest: 'build/robots.txt'
          },
          index: {
              src: 'build/snapshots/index.html',
              dest: 'build/snapshots/.html'
          }
      },
      cssmin: {
          main: {
              src: 'src/css/custom.css',
              dest: 'build/css/custom.css'
          }
      },
      staticinline: {
          main: {
              files: {
                  'build/index.html': 'build/index.html'
              }
          }
      },
      useminPrepare: {
          html: 'src/index.html',
          options: {
              staging: '<%= conf.temp %>',
              dest: 'build'
          }
      },
      rev: {
          files: {
              src: ['build/js/app.js']
          }
      },
      usemin: {
          html: 'build/index.html'
      },
      cdnify: {
          main: {
              options: {
                  rewriter: function (url) {
                      if (url == 'css/bootstrap.css')
                          return '//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css';
                      else if (url == 'js/socket.io.js')
                          return '//cdnjs.cloudflare.com/ajax/libs/socket.io/0.9.6/socket.io.min.js'
                      else if (url == 'js/angular.js')
                          return '//ajax.googleapis.com/ajax/libs/angularjs/1.2.10/angular.min.js'
                      else if (url == 'js/angular-route.js')
                          return '//ajax.googleapis.com/ajax/libs/angularjs/1.2.10/angular-route.min.js'
		      else if (url == 'js/d3.js')
			  return "//cdnjs.cloudflare.com/ajax/libs/d3/3.4.1/d3.min.js"
		      else if (url == 'js/cubism.v1.js')
			  return "//cdnjs.cloudflare.com/ajax/libs/cubism/1.2.2/cubism.v1.min.js"
                      else if (url.indexOf('.app.js') === 11)
                          return s3 + url
                      else
                          return url;
                  }
              },
              files: [{
                  expand: true,
                  cwd: 'build',
                  src: '**/*.{css,html}',
                  dest: 'build'
              }]
          }
      },
      replace: {
          sockio: {
              src: ['build/index.html'],
              overwrite: true,
              replacements: [
		  {
                      from: 'WEB_SOCKET_SWF_LOCATION = "bin/WebSocketMain.swf"',
                      to: 'WEB_SOCKET_SWF_LOCATION = <%= conf.s3 %>bin/WebSocketMain.swf"'
		  },
		  {
                      from: 'WEB_SOCKET_DEBUG = true',
                      to: 'WEB_SOCKET_DEBUG = false'
		  }
	      ]
          }
      },
      'escaped-seo': {
          main: {
              options: {
                  domain: 'http://chartfactory.com',
                  server: 'http://dev.chartfactory.com',
                  public: 'build',
                  folder: 'snapshots',
                  changefreq: 'daily',
                  delay: 2000,
                  replace: {
                      '': /<script.*<\/script>/gim
                  }
              }
          }
      }
  });

  grunt.loadNpmTasks('grunt-rev');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-usemin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-static-inline');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-cdnify');
  grunt.loadNpmTasks('grunt-text-replace');
  grunt.loadNpmTasks('grunt-escaped-seo');

  // Default task(s).
  grunt.registerTask('default', [
      'clean:init',
      'copy:html',
      'copy:fav',
      'copy:rob',
      'cssmin',
      'staticinline',
      'useminPrepare',
      'concat',
      'uglify',
      'rev',
      'usemin',
      'cdnify',
      'replace:sockio',
      'escaped-seo',
      'copy:index',
      'clean:exit'
  ]);
};
